import { useState, useEffect } from "react";
import { Routes, Route, Link } from "react-router-dom";
import "./App.css";

// have to use oats.lol as redirect_uri b/c cognito isn't configured w/ localhost
// todo configure cognito w/ localhost
const OatsHostedUIURI = `https://jjkis.auth.us-east-1.amazoncognito.com/login?client_id=3tm2ihl6i15elk1cqgaj6glagl&response_type=token&scope=email+openid+phone&redirect_uri=https://oats.lol/login`

const HostedUIURI = OatsHostedUIURI

function App() {
  const [token, setToken] = useState(localStorage.getItem("accessToken"));

  useEffect(() => {
    // storing input name
    const urlSearchParams = new URLSearchParams(window.location.hash);
    const params = Object.fromEntries(urlSearchParams.entries());
    console.table(params);
    if (params["access_token"]) {
      localStorage.setItem("accessToken", params["access_token"]);
      window.location.hash = "";
      setToken(params["access_token"]);
    }
  },[]);

  if (token) {
    return (
      <div className="App">
        <h1>Welcome to React Router!</h1>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/login">login</Link>
            </li>
            <li>
              <Link to="/logout">logout</Link>
            </li>
          </ul>
        </nav>

        <Routes>
          <Route path="/logout" element={<Logout setToken={setToken} />} />
          <Route path="/" element={<Home token={token} />} />

        </Routes>
      </div>
    );
  }
  window.location.href = HostedUIURI;
  return null;
}

function Logout ({setToken}:{setToken:  React.Dispatch<React.SetStateAction<string | null>>}) {
  useEffect(() => {
    localStorage.removeItem("accessToken");
    setToken(null);
    window.location.href = HostedUIURI;
  },[setToken])
  return null;
}


function Home({ token }: { token: string }) {
  const [error, setError] = useState<null | any>(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState<Array<any>>([]);

  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()
  useEffect(() => {
    // todo parameterize me
    fetch("https://oms.jjk.is/files", {
      method: "GET",
      credentials: "include",
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
      return () => {
        setItems([]);
        setIsLoaded(false);
        setError(null);
      }
  }, [token]);

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <ul>
        {items.map((item: any, i) => (
          <li key={i}>{item}</li>
        ))}
      </ul>
    );
  }
}
export default App;
